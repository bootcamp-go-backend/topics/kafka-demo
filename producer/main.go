package main

import (
	"github.com/IBM/sarama"
	"time"
)

func main() {
	kafkaCfg := sarama.NewConfig()
	kafkaCfg.Producer.Return.Successes = true
	kafkaCfg.Producer.Return.Errors = true

	client, err := sarama.NewClient([]string{"localhost:9092"}, kafkaCfg)
	if err != nil {
		panic(err)
	}
	defer client.Close()

	broker, err := client.Controller()
	if err != nil {
		panic(err)
	}
	defer broker.Close()

	if _, err = broker.CreateTopics(&sarama.CreateTopicsRequest{
		TopicDetails: map[string]*sarama.TopicDetail{
			"say-hello": {
				NumPartitions:     1,
				ReplicationFactor: 1,
			},
		},
	}); err != nil {
		panic(err)
	}

	producer, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	if _, _, err = producer.SendMessage(&sarama.ProducerMessage{
		Topic:     "say-hello",
		Key:       sarama.StringEncoder("Key"),
		Value:     sarama.StringEncoder("Val"),
		Timestamp: time.Now(),
	}); err != nil {
		panic(err)
	}
}

// TOPIC: partition
// pay-to-courier: 0 => courier_id1, amount1, order_id1 | offset 0
//                 0 => courier_id2, amount2, order_id2 | offset 1
// 				   1 => courier_id3, amount3, order_id3 | offset 0
//                 1 => courier_id4, amount2, order_id4 | offset 1
// ....
