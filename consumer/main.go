package main

import (
	"fmt"
	"github.com/IBM/sarama"
)

func main() {
	kafkaCfg := sarama.NewConfig()
	kafkaCfg.Consumer.Return.Errors = true

	client, err := sarama.NewClient([]string{"localhost:9092"}, kafkaCfg)
	if err != nil {
		panic(err)
	}

	consumer, err := sarama.NewConsumerFromClient(client)
	if err != nil {
		panic(err)
	}

	partitionConsumer, err := consumer.ConsumePartition("say-hello", 0, 0)
	if err != nil {
		panic(err)
	}

	fmt.Println("waiting messages...")
	for msg := range partitionConsumer.Messages() {
		fmt.Printf("key: %s, value: %s, time: %v\n", msg.Key, msg.Value, msg.Timestamp)
	}
}
